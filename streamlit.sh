#!/bin/bash

cd /workspace
cd $COMPONENT_FOLDER
pip install streamlit
streamlit run __init__.py --server.port=8501 --server.address=0.0.0.0