#!/bin/bash

cd /workspace
cd $COMPONENT_FOLDER
cd frontend
npm install
CI=true npm start run --host "0.0.0.0" --port "3001"