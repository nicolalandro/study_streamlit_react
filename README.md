# Study Streamlit Component with React
Guide/study to develop pluigin for streamlit.

## Dev with docker
```
docker-compose up
```

## Local Develop
### dev python
```
poetry init
poetry install

poetry run streamlit run my_component/__init__.py
```

### dev node
```
cd my_component/frontend/                                                                                                                                                      
source ~/.asdf/asdf.fish
asdf local nodejs 18.9.1 
npm install
```

# References
* python
* poetry
* asdf
* nodejs
* [fix](https://stackoverflow.com/questions/69719601/getting-error-digital-envelope-routines-reason-unsupported-code-err-oss)
* react
* [streamlit component](https://docs.streamlit.io/library/components/create)
* docker
* docker-compose